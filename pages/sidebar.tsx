import type { NextPage } from "next";
import Head from "next/head";

const Sidebar: NextPage = () => {
  return (
    <>
      <Head>
        <title>Sidebar - TS Projects</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <div className="min-h-screen p-4 bg-grey-900 flex justify-center items-center">
        This is sidebar
      </div>
    </>
  );
};

export default Sidebar;
